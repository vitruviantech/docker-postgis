#!/bin/sh

set -e

# Perform all actions as $POSTGRES_USER
export PGUSER="$POSTGRES_USER"

# Create the 'template_plv8' template db
"${psql[@]}" <<- 'EOSQL'
CREATE DATABASE template_plv8 IS_TEMPLATE true;
EOSQL

# Load PLV8 into both template_database and $POSTGRES_DB
for DB in template_postgis "$POSTGRES_DB"; do
	echo "Loading PLV8 extensions into $DB"
	"${psql[@]}" --dbname="$DB" <<-'EOSQL'
		CREATE EXTENSION IF NOT EXISTS plv8;
    SELECT plv8_version();
EOSQL
done