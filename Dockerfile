FROM postgis/postgis:13-master

RUN apt update && \
    apt install --no-install-recommends -y elpa-magit && \
    apt install --no-install-recommends -y build-essential wget git-all g++ python2 pkg-config libc++-dev libc++abi-dev libglib2.0-dev libtinfo5 ninja-build postgresql-server-dev-13 && rm -rf /var/lib/apt/lists/* && \
    ln -s $(which python2) /bin/python && \
    wget https://github.com/plv8/plv8/archive/v3.0.0.tar.gz && \
    tar -xvzf v3.0.0.tar.gz && \
    cd plv8-3.0.0 && make && make install && \
    rm -rf /v3.0.0.tar.gz /plv8-3.0.0 && \
    apt remove -y elpa-magit build-essential wget git-all g++ python2 pkg-config libc++abi-dev libglib2.0-dev libtinfo5 ninja-build postgresql-server-dev-13 && \
    apt clean -y && \
    apt autoremove -y && \
    apt --no-install-recommends -f install -y

COPY ./initdb-plv8.sh /docker-entrypoint-initdb.d/11_plv8.sh